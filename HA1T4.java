/*
 * Write a program that tests the performance difference of LinkedList and ArrayList. 
 * The algorithm should add n elements to the list, traverse the elements, and remove the elements one by one. 
 * Write a single algorithm that uses operations of interface List operations and then write a test method that calls the algorithm using both list implementations.
 */

import java.util.*;

public class HA1T4 {

	public static void main(String[] args) {

		int N = 10;
		long timeStart,timeEnd; 
		if (args.length > 0)
			N = Integer.valueOf(args[0]);
		    int seed = 1;
		    if (args.length > 1)
		        seed = Integer.valueOf(args[1]);
		    
		List<Object> L = new LinkedList<Object>();
		List<Object> A = new ArrayList<Object>();
		
		for(N = 10; N <= 100; N = N + 10) {
			Object[] array = new Object[N];
			Integer[] arrayInteger = new Integer[N];
			int[] arrayInt = new int[N];
			timeStart = System.nanoTime(); 
			traverseList(N,seed,L); 
			timeEnd = System.nanoTime();
			System.out.println("["+N+"] LinkedList: " + (timeEnd-timeStart) + " ns");
			timeStart = System.nanoTime(); 
			traverseList(N,seed,A); 
			timeEnd = System.nanoTime(); 
			System.out.println("["+N+"] ArrayList: " + (timeEnd-timeStart) + " ns");
			timeStart = System.nanoTime();
			traverseArray(array,N,seed);
			timeEnd = System.nanoTime();
		    System.out.println("["+N+"] Array(Object): " + (timeEnd-timeStart) + " ns");
			timeStart = System.nanoTime();
  			traverseArrayInteger(arrayInteger,N,seed);
  			timeEnd = System.nanoTime();
  		    System.out.println("["+N+"] Array(Integer): " + (timeEnd-timeStart) + " ns");
 
 		    timeStart = System.nanoTime(); 
 			traverseArrayInt(arrayInt,N,seed); 
 			timeEnd = System.nanoTime(); 
 			System.out.println("["+N+"] Array(int): " + (timeEnd-timeStart) +  " ns\n");
 			
		}
	}
	
	public static List<Object> randomList(int n,int seed,List<Object> V) {
		Random r = new Random(seed);
		for(int i = 0; i < n; i++)
			V.add(i,r.nextInt(n));
		return V;
	}
	public static void traverseArrayInt(int[] array, int seed, int n ) {
		Random r = new Random(seed); 
		for(int i = 0; i < n; i++) {
			array[i] = r.nextInt(n);
		}
		for(int i = 0; i < array.length; i++) {
			array[i] = 0;
		}
	}
	public static void traverseArrayInteger(Integer[] array, int seed, int n) {
		Random r = new Random(seed);
		for(int i = 0; i < n; i++) {
			array[i] = (Integer)r.nextInt(n);
		}
		for(int i = 0; i < array.length; i++) {
			array[i] = null;
		}
	}
	
	public static void traverseArray(Object[] array,int seed, int n) {
		Random r = new Random(seed);
		for(int i = 0; i < n; i++)
			array[i]= r.nextInt(n);
		for(int i = 0; i < array.length; i++) {
			array[i] = null;
		}
    }
		
	public static void traverseList(int n, int seed, List<Object> list) {
		Random r = new Random(seed);
		for(int i = 0; i < n; i++)
			list.add(i,r.nextInt(n));
		
		Iterator<Object> iter = list.iterator(); 
		while(iter.hasNext()) {
			Object o = iter.next(); 
			iter.remove();
		}
	}
}

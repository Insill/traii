import java.util.Stack;
import java.util.StringTokenizer;

import javax.swing.JOptionPane;

public class ONP {

    public static void main(String[] args) {
        Stack<Double> stack = new Stack<Double>();
        double number1;
        double number2;
		String s1; 
		
        String x = JOptionPane.showInputDialog("Tokens");
        StringTokenizer str = new StringTokenizer(x);

        while (str.hasMoreElements()) 
		{

		s1 = str.nextElement();

		 if(s1.equals("+") || s1.equals("-") || s1.equals("*") || s1.equals("/"))
		 {
			  switch (s1) 
			  {
					case '+':
						number1 = stack.pop();
						number2 = stack.pop();
						System.out.print(number1 + " + " + number2);
						break;
					case '-':
						number1 = stack.pop();
						number2 = stack.pop();
						System.out.print(number1 +" - "+ number2);
						break;
					case '/':
						number1 = stack.pop();
						number2 = stack.pop();
						System.out.print(number1 +" / "+ number2);
						break;
					case '*':
						number1 = stack.pop();
						number2 = stack.pop();
						System.out.print(number1 +" * "+ number2);
						break;
					}
			 }
			 else
			 {
					 stack.push(s1);
			 }
			 
            }
            System.out.println(stack.pop());
        }
    }


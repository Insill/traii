/**
  *
  * 2. Write an algorithm that finds which element of a given collection has
  * most occurrences. The parameter is a Collection<E> and the return value is
  * an element (of type E) that has most occurrences in the collection. If
  * there are multiple elements with equal number of occurrences, return any of
  * those. If the collection is empty, return null. Compare elements with
  * .equals() operation. Use a mapping (Map<E, Integer>) as a helping
  * structure. What is the time complexity of your algorithm?
  *
  * 3. Compare the running time of task 2 when the helping data structure is
  * (a) HashMap, or (b) TreeMap. Write a program that measures the time
  * difference of these when input grows. How you explain the results?
  *
  **/

import java.util.*;

public class HA1T2 {

	public static void main(String[] args) {
        // number of elements
		Integer most; 
		long timeStart, timeEnd; 
		int N = 10;
		if (args.length > 0)
			N = Integer.valueOf(args[0]);
	
	    // random number seed
	    int seed = 1;
	    if (args.length > 1)
	        seed = Integer.valueOf(args[1]);
	
	    // element list
/*	    LinkedList<Integer> M = randomLinkedList(N, seed);
	
	    // print list if only few elements
	    if (N <= 50) {
	        System.out.print("L (input size "+N+")");
	        for (Integer x : M)
	            System.out.print(" " + x);
	        System.out.println();	        
        }	
	    Integer most = mostT(M);
        most = mostH(M);
*/        
        for(N = 100; N <= 1000; N = N+50) {
	        LinkedList<Integer> L = randomLinkedList(N, seed);
	        System.out.println(N);
	        // TODO: measure timing for task 3
	        timeStart = System.nanoTime();
	        most = mostH(L);
	        timeEnd = System.nanoTime();
	        System.out.println("[HashMap]Most occurrences was " + most +"; " + (timeEnd-timeStart) +" ns");
	        
	        timeStart = System.nanoTime();
	        most = mostT(L);
	        timeEnd = System.nanoTime();
	        System.out.println("[TreeMap]Most occurrences was " + most+"; " +(timeEnd-timeStart) +" ns\n");
        }
        // for better testing, call several times, take minimum
        // iterate over different input sizes
		
	}
    public static <E> E mostH(Collection<E> C) {
        // allocating hash table helps a bit
        Map<E, Integer> mapping = new HashMap<>((int)(1.6*C.size()));
        E maxelem = null;
        
        for(E e: C) {
        	Integer value = mapping.get(e); 
        	if(value == null) {
        		mapping.put(e, 1); 
        	} else {
        		mapping.put(e,value+1);
        	}
        	
        }
        for(Map.Entry<E, Integer> e: mapping.entrySet()) {
        	if(maxelem == null) {
        		maxelem = e.getKey();
        		continue; 
        	}
        	if(e.getValue() > mapping.get(maxelem)) {
        		maxelem = e.getKey();
        	}
        }
        
        // TODO task 2
        return maxelem;

    } // mostH()
    
    public static <E extends Comparable<? super E>> E mostT(Collection<E> C) {
    	TreeMap<E, Integer> mapping = new TreeMap<>();

    	E maxelem = null;
 
        for(E e: C) {
        	Integer value = mapping.get(e); 
        	if(value == null) {
        		mapping.put(e, 1); 
        	} else {
        		mapping.put(e,value+1);
        	}
        	
        }
        for(Map.Entry<E, Integer> e: mapping.entrySet()) {
        	if(maxelem == null) {
        		maxelem = e.getKey();
        		continue; 
        	}
        	if(e.getValue() > mapping.get(maxelem)) {
        		maxelem = e.getKey();
        	}
        }
        
        // TODO task 3
        return maxelem;

    }
    
 /**
  * Creates random integer list.
  * @param n number of elements
  * @param seed random number seed
  * @return new list
  **/
   public static LinkedList<Integer> randomLinkedList(int n, int seed) {
       Random r = new Random(seed);
       LinkedList<Integer> V = new LinkedList<Integer>();
       for (int i = 0; i < n; i++)
           V.add(r.nextInt(n));
       return V;
   }
       
}

import fi.joensuu.cs.tra.*;
import java.util.LinkedList;

/*
 * Mielestäni onnistuin X-tehtävässä melko hyvin ja tekemisessä kesti vain pari päivää; pelkäsin tehtävän olevan vaikeampi ja enemmän aikaavievä. 
 * Mitä oikeellisuuteen tulee, en keksinyt miten tulostaa painavin polku solmuittain, paino tulostuu sentään oikein.
 * Ratkaisuani voisi kai pitää modulaarisena koska siinä toiminta on jaettu kolmeen metodiin. On kätevämpää tarkistaa, löytyykö juuresta lähtevää puuta verkosta ja samalla
 * koota tätä puuta sen sijaan kuin tekisi näistä erillisiä. Erillisenä tämä toiminnallisuus veisi enemmän aikaa ja olisi redundanttia. 
 * 
 * Aikavaativuudeltaan verkon läpikäynti ja puun etsiminen isCyclic-metodissa on O(solmut + kaaret) silloin kun kaaria on solmuja enemmän.
 * Kun kaaria on enemmän, aikavaativuus on O(kaaret). Metodi on käytännössä syvyyssuuntainen haku, jossa katsotaan verkkoa läpi solmu kerrallaan ja katsotaan läpi solmun naapurit.
 * Kun rootVertices-metodissa iteroidaan läpi ensiksi kaaria ja sitten solmuja, molemmissa aikavaativuus on O(n).
 * Olen tottunut kommentoimaan koodia sekä koulussa että vapaa-ajalla englanniksi joten ratkaisuni kommentit ovat sillä kielellä.
 * 
 */

public class X1 {


	public static void main(String[] args) {

	    int vertices = 9;
        int edges = 6;

        // first parameter: number of edges
        if (args.length > 0)
            vertices = Integer.valueOf(args[0]);

        // second parameter: number of edges
        if (args.length > 1)
            edges = Integer.valueOf(args[1]);

        int seed = vertices+edges;

        // third parameter: random number seed
        if (args.length > 2)
            seed = Integer.valueOf(args[2]);
    
        // change to your username also here
        X1 y = new X1();

        // create random graph
        DiGraph graph = GraphMaker.createDiGraph(vertices, edges, seed);
        System.out.println("\nGraph:");

        GraphMaker.setWeights(graph, 10, (float)0.1, seed);

        // GraphMaker.toString(graph, 1) prints edges in form "d-w",
        // where d is the destination vertex and w is weight of edge
        if (edges < 20)
            System.out.println(GraphMaker.toString(graph, 1));

        System.out.println("Roots: " + y.rootVertices(graph));

        LinkedList<Vertex> heaviest = new LinkedList<Vertex>();
        float mw = y.heaviestTree(graph, heaviest);
        System.out.println("heaviestTree: " + mw + " : " + heaviest);

        // create directed acyclig graph
        graph = GraphMaker.createDAG(vertices, edges, seed);
        System.out.println("\nDAG:");

        GraphMaker.setWeights(graph, 10, (float)0.1, seed);
        if (edges < 20)
            System.out.println(GraphMaker.toString(graph, 1));

        System.out.println("Roots: " + y.rootVertices(graph));

        heaviest = new LinkedList<Vertex>();
        mw = y.heaviestTree(graph, heaviest);
        System.out.println("heaviestTree: " + mw + " : " + heaviest);


        // create forest, useful to use edges < vertices
        graph = GraphMaker.createForest(vertices, edges, seed, 10);
        System.out.println("\nForest:");

        GraphMaker.setWeights(graph, 10, (float)0.1, seed);
        if (edges < 20)
            System.out.println(GraphMaker.toString(graph, 1));

        System.out.println("Roots: " + y.rootVertices(graph));

        heaviest = new LinkedList<Vertex>();
        mw = y.heaviestTree(graph, heaviest);
        System.out.println("heaviestTree: " + mw + " : " + heaviest);

        // mess the forest
       
        System.out.println("\nMessed forest 1");
        GraphMaker.addRandomEdges(graph, 1, false, (float)10);
        if (edges < 20)
            System.out.println(GraphMaker.toString(graph, 1));

        System.out.println("Roots: " + y.rootVertices(graph));


        heaviest = new LinkedList<Vertex>();
        mw = y.heaviestTree(graph, heaviest);
        System.out.println("heaviestTree: " + mw + " : " + heaviest);
       
        System.out.println("\nMessed forest 2");
        GraphMaker.addRandomEdges(graph, 2, false, (float)10);

        if (edges < 20)
            System.out.println(GraphMaker.toString(graph, 1));

        System.out.println("Roots: " + y.rootVertices(graph));

        heaviest = new LinkedList<Vertex>();
        mw = y.heaviestTree(graph, heaviest);
        System.out.println("heaviestTree: " + mw + " : " + heaviest);
       

        System.out.println();

	}
	
   float heaviestTree(DiGraph G, LinkedList<Vertex> resultTree) {
	   
	   Set<Vertex> R = rootVertices(G); 
	   float heaviest = 0;
	   float[] weight = new float[1]; // interestingly the value of a float variable doesn't carry over to methods but it works when using a float array. Weird.
	   for(Vertex v: R) { // we have root nodes now, let's iterate over the set!  
		   if(isTree(v, G,weight,resultTree) == true) {
	   			if(weight[0] > heaviest ) {
	   				heaviest = weight[0]; 
	   			}
		   }
	   }
	   if(heaviest != Float.NaN) { 
		   return heaviest;   
	   }
	   else {
		   return Float.NaN;
	   }
	   // DO NOT try to write everything here, it is not possible (or practical).
	   // Plan modular solution (several methods) according to your algorithm designed
	   // on paper.
	
	   // If you do not have a planned algorithm now, please shut down your computer and
	   // return to the drawing board.
	
	   // When you have the plan, follow it without making any unplanned hacks.
	   // If you need to add something, you must be able to reason it for yourself.

    } 
	
   	boolean isTree(Vertex root, DiGraph g, float[] weight,LinkedList<Vertex> resultTree) {
   	 		
   		for(Vertex v: g.vertices()) { // every vertex is colored white so that the graph can be traversed
   			v.setColor(DiGraph.WHITE); // just like starting a depth-first search 
   		}
   		
		if(isCyclic(root,g,weight,resultTree) == true) { 
			return false;
		}
		
		return true;
   	}   	
   	// if there is a cycle, this isn't a tree 

   	boolean isCyclic(Vertex v, DiGraph g,float[] weight,LinkedList<Vertex> resultTree) {
   		v.setColor(DiGraph.BLACK);
   		for(Vertex neighbor: v.neighbors()) { // recursive and like depth-first search
   			if(neighbor.getColor() == DiGraph.WHITE) { 
   				Edge e = v.getEdge(neighbor);
   				weight[0] += e.getWeight();
   				return isCyclic(neighbor,g,weight,resultTree);
   			}
   			else {
   				return true; 
   			}
   		}
		return false;
   		
   	}
   	
    Set<Vertex> rootVertices(DiGraph g) {
    	
    	Set<Vertex> R = new Set<Vertex>(); 
    	Set<Vertex> rAll = new Set<Vertex>(); 
    	
    	for(Edge e: g.edges()) {  
    		rAll.add(e.getEndPoint());
    	}
    	for(Vertex v: g.vertices()) { 
    		if(!rAll.contains(v)) {
    			R.add(v);
    		}
    	}

        return R;
    }   // rootVertices()
 

}

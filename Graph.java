import java.util.*;

import java.io.*;

public class Graph<L,W> {
	
	private final static int white = 0;
	private final static int grey = 1; 
	private final static int black = 2; 
	int counter = 0; 
	Queue<Vertex> queue = new LinkedList<Vertex>();
	
	protected class Vertex {
		L label;
		int color; 
		LinkedList<Edge> adjacent;

		Vertex (L label) {
			this.label = label;
			adjacent = new LinkedList<Edge>();
		}
	}

	protected class Edge {
		Vertex head;
		W	weight;

		Edge (Vertex head, W weight) {
			this.head = head;
			this.weight = weight;
		}
	}

	protected LinkedHashMap<L,Vertex>	vertices;

	Graph () { vertices = new LinkedHashMap<L,Vertex> (); }

	public Vertex getVertex (L label) {
		return vertices.get (label);
	}

	public Vertex addVertex (L label) {
		Vertex v = vertices.get (label);

		if (v == null) {
			v = new Vertex (label);
			vertices.put (label, v);
		}

		return v;
	}

	public void addEdge (L u, L v, W weight) {
		Vertex uu = addVertex (u);
		Vertex vv = addVertex (v);
		uu.adjacent.add (new Edge (vv, weight)); // u --> v
	//	vv.adjacent.add (new Edge (uu, weight)); // u <-- v 
	}

	@SuppressWarnings("unchecked")
	public void kohtalot () throws Exception {
    
		Graph<Integer,Integer> graph = new Graph<Integer,Integer>(); 
		File file = null; 
		String stringPath = null, filePath = null;
		Scanner input = new Scanner(System.in); // Kysyt��n tiedostonime�
		
		try{
			System.out.print("Anna tiedoston polku (Esim: C:\\abcfolder\\textfile.txt): ");
			stringPath = input.next();
			filePath = new File(stringPath).getAbsolutePath();
			file = new File(filePath);
			input = new Scanner(file); 	
			
		} catch(Exception e) {
			e.printStackTrace();	 
		}
		
		int amount = input.nextInt(); // Luetaan ensimm�inen numero, joka kertoo, kuinka monta tapahtumaa eli rivi� on 
		input.nextLine(); // Hyp�t��n Scannerilla seuraavaan riviin jossa on varsinainen luettava sy�te 
		
		for(int i = 0; i < amount; i++) {
			graph.addVertex(i); //Lis�t��n solmu graafiin
			String numberString = input.nextLine();  // Otetaan rivi stringin�, laitetaan se String-taulukkoon, joka muutetaan int-taulukoksi alempana
			String[] StringArray = numberString.split(" "); 
			int[] numbers = new int[StringArray.length]; 
			System.out.print("\n--------\nNumberString: " + numberString +"\nNumbers.length: " + numbers.length);
			
			try {	 
				for(int j = 0; j < StringArray.length; j++){	
						String string = StringArray[j]; 	    // Tajusin, mist� ArrayIndexOutOfBoundsException johtuu: koko luupissa ollaan isommassa luvussa kuin mit� taulukkoon mahtuu
						numbers[j] = Integer.parseInt(string);	// Esim. taulukon koko on 1, kun ollaan jo rivill� 3, iteroidessa pit�� olla muu muuttuja kuin i! 
					}
				}catch(Exception e) {
						e.printStackTrace();
				}

			// Iteroidaan rivist� saamamme taulukko l�pi ja luodaan kaaret 
			// K�yd��n l�pi taulukko alkiosta 1 alkaen koska alkio 0 kertoo, kuinka monta valintaa meill� on (paitsi kun taulukossa on vain 0, silloin on "kuolema")
			
			int l; 
			if(numbers.length == 1) { l = 0; } else { l = 1;} 
			
			for(int j = l; j < numbers.length; j++) {
			
				int edgeCount; 
				if(numbers[0] == 0) {edgeCount = 1;} else {edgeCount = numbers[0]; }
				int startPos = j; 
				
				System.out.println("\n-------\n" +"CurNode: " + i + "  EdgesCount: " + edgeCount + " StartPosition: " + startPos + "  Numbers.length: " + numbers.length + "  i: " + i + "   j: numbers[" + j+"]\n----------");
				for (int k = startPos; k < edgeCount + startPos; k++) {
						graph.addEdge(i, numbers[j], 1);
						System.out.println(i + " --> " + numbers[j]);
				}

			}
		}	
		input.close();
		Dfs(graph, (Vertex)graph.getVertex(0)); 
		System.out.println("\n"+counter);
	}
	
	// Syvyyssuuntainen l�pik�ynti. Koska graafi alkaa nollasta ja my�s p��ttyy nollaan, on sanomattakin selv��, ett� aloitussolmu on 0
	public void Dfs(Graph graph, Vertex start) {
		Collection <Vertex> s = graph.vertices.values(); 
		System.out.print("\n"+start.label);
		for(Vertex v: s) {
			v.color = white; 
		}
		for(Edge e: start.adjacent) {
			if(start.color == white) {
				DfsVisit(start); 
			}
		}
	}	

	public void DfsVisit(Vertex v) {
		v.color = grey; 
		for (Edge e: v.adjacent) {
			if(v.color == white) {
				DfsVisit(v);
			}
		}
		v.color = black;	
		if((int) v.label == 0) {
			counter++; 
		}
		System.out.print(" --> " + v.label);
	}
	
	public static void main (String [] args) throws Exception {
		Graph graph = new Graph(); 
		graph.kohtalot();
	}
}



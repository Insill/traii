import java.util.*;

public class RandomPi {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in); 
		System.out.print("Kuinka monta kertaa tikkaa heitet��n?:\t");
		int dartThrows = scanner.nextInt();
		double pi = calculatePi(dartThrows);
		double difference = pi - Math.PI; 
		System.out.println("Tikkaa heitettiin " + dartThrows + " kertaa ja laskettu pii on " + pi + ". Javasta vakiona l�ytyv��n piihin verrattuna ero on " + difference);
		scanner.close(); 
	}
	
	public static double calculatePi(int dartThrows) {
		Random random = new Random(System.currentTimeMillis()); 
		double pi = 0; 
		int hits = 0; 
		// Aikavaativuus?  
		for(int i = 1; i <= dartThrows; i++) {
			double x = (random.nextDouble()) * 2 - 1.0;
			double y = (random.nextDouble()) * 2 - 1.0; 
			
			if(Math.sqrt((x * x ) + (y*y)) < 1.0) {
				hits++; 
			}
			
		}
		double doubleThrows = dartThrows; 
		pi = (4.0 * (hits / doubleThrows));
		return pi; 
	}
}

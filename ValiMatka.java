import java.io.*;
import java.util.*;


public class ValiMatka {
	
	static ArrayList<String> cities = new ArrayList<String>(); 
	static int adjacencyMatrix[][]; 
	static int copyMatrix[][]; 
	static int answer[][]; 
	final static int INF = 999;
	static int stringMaxLength = 6; 

	
	public static void main(String[] args) {
		
		String city1,city2 = null; 
		File file = null; 
		int distance;  
		String stringPath = null, filePath = null;
		Scanner input = new Scanner(System.in); // Kysyt��n tiedostonime�

		
		try {
			System.out.print("Anna tiedoston polku (Esim: C:\\abcfolder\\textfile.txt): ");
			stringPath = input.next();
			filePath = new File(stringPath).getAbsolutePath();
			file = new File(filePath);
			input = new Scanner(file); 	
			
		} catch(Exception e) {
			e.printStackTrace();	 
		}
		
		int citiesAmount = input.nextInt();	// Luetaan ensimm�iselt� rivilt� kaupunkien ja polkujen m��r�
		int roadsAmount = input.nextInt();
		adjacencyMatrix = new int[citiesAmount][citiesAmount]; // Tehd��n vierusmatriisi kaupungeista ja niiden v�lisist� et�isyyksist�
		
		for(int i = 0; i < adjacencyMatrix.length; i++) { // K�tevin ratkaisu olisi k�ytt�� Arrays.fill -metodia mutta se toimii vain yksiulotteisille taulukoille. Thanks Obama!
			for(int j = 0;  j < adjacencyMatrix.length; j++) {
				if(i == j && j == i) {
					adjacencyMatrix[i][j] = 0; // Kahdella samalla kaupungilla et�isyys on tietysti 0 
				}
				else {
					adjacencyMatrix[i][j] = INF; // Laitetaan intin suurin arvo ilmaisemaan ikuisuutta eli solmuja, joilla ei ole yhteytt� toisiinsa
					adjacencyMatrix[j][i] = INF;
				}
			}
		}
			
		input.nextLine(); 
		
		// Luodaan ArrayList kaupunkien nimist�, aluksi k�ytin HashMapia mutta se ei ole kelvollinen jos halutaan alkiot j�rjestyksess�
		for(int i = 0; i < citiesAmount; i++) {
			String temp = input.next(); 
			if(temp.length() > stringMaxLength) { // P�tk�ist��n kaupungin nimest� lyhyempi jotta lopussa matriisin tulostuksesta saadaan teht�v�nannon esimerkin mukainen 
				temp = temp.substring(0,stringMaxLength);
			}
			cities.add(temp);	 
		}	
		
		// Luetaan kaaret sis�lt�v�t rivit eli "kaupunki1 kaupunki2 et�isyys"
		for(int i = 0; i < roadsAmount; i++) {

				city1 = input.next(); 
				city2 = input.next(); 
				if(city1.length() > stringMaxLength) {
					city1 = city1.substring(0,stringMaxLength);
				}
				if(city2.length() > stringMaxLength) {
					city2 = city2.substring(0,stringMaxLength); 
				}
				distance = input.nextInt();
			
				adjacencyMatrix[cities.indexOf(city1)][cities.indexOf(city2)] = distance; // Asetetaan kahden eri kaupungin v�linen et�isyys
				adjacencyMatrix[cities.indexOf(city2)][cities.indexOf(city1)] = distance; 
		}
		
		city1 = input.next(); // Otetaan viimeiselt� rivilt� talteen kaksi kaupunkia joiden v�linen lyhin reitti tahdotaan selvitt�� 
		city2 = input.next(); 
		if(city1.length() > stringMaxLength) {
			city1 = city1.substring(0,stringMaxLength);
		}
		if(city2.length() > stringMaxLength) {
			city2 = city2.substring(0,stringMaxLength); 
		}
		
		System.out.println();
		input.close(); 
		
		FloydWarshall(adjacencyMatrix); // Kutsutaan Floyd-Marshallin algoritmia
		System.out.print("\t"); 
		for(String city: cities ) {
			System.out.print(city + "\t");
		}
		System.out.println();
		printMatrix(adjacencyMatrix); // Tulostetaan tuloksena syntynyt matriisi, ylempi on aika k�mpel� ratkaisu mutta laitetaan silti 
		System.out.println();
		shortestPath(cities.indexOf(city1),cities.indexOf(city2)); // Selvitet��n lyhin reitti
	}
	
	public static int[][] FloydWarshall(int[][] adjacencyMatrix) {
			for(int i = 0; i < adjacencyMatrix.length; i++) {
				for(int j = 0; j < adjacencyMatrix.length; j++) {
					for(int k = 0; k < adjacencyMatrix.length; k++) {
							adjacencyMatrix[j][k] = Math.min(adjacencyMatrix[j][k], adjacencyMatrix[j][i] + adjacencyMatrix[i][k]);
							adjacencyMatrix[k][j] = Math.min(adjacencyMatrix[j][k], adjacencyMatrix[j][i] + adjacencyMatrix[i][k]);
					}
				}
		}
		
		return adjacencyMatrix;
	}
	
	public static void printMatrix(int[][] adjacencyMatrix) {	
		for(int i = 0; i < adjacencyMatrix.length; i++) {
			System.out.print(cities.get(i) + "\t");
			for(int j = 0; j < adjacencyMatrix.length; j++) {
					System.out.print(adjacencyMatrix[i][j] + "\t"); 
			}
			System.out.println(); 
		}
	
	}
	
	public static void shortestPath(int city1,int city2) {	// Ei toimi
		String path = " "; 
		while(adjacencyMatrix[city1][city2] != city2) {
			path = adjacencyMatrix[city1][city2] + " " + path; 
			city2 = adjacencyMatrix[city1][city2]; 
		}
		System.out.println(cities.get(city1) + path); 
	/*
		shortestPath(city1,adjacencyMatrix[city1][city2]);
		System.out.println(" " + adjacencyMatrix[city1][city2]);
		shortestPath(adjacencyMatrix[city1][city2],city2);
	 */
		
	}
}

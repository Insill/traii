import java.util.*;
public class DIYSqrt {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in); 
		double input = 0; 
		System.out.print("Anna luku, josta lasketaan neliöjuuri:\t");
		input = scanner.nextDouble(); 
		scanner.close();
	 	double result = square(1, input,input); 
	 	System.out.println(result); 
	 	
	}
	
	public static double square(double left, double right, double x ) {
		// Aikavaativuus 
		
		if (x == 0 || x == 1 ) {
			return x; 
		}
		double middle = (left + right ) / 2; 
		
		if (middle * middle  <= x && (middle+1)*(middle+1) > x ) {
			return middle; 
		}
		
		if (middle * middle < x ) {
			return square(middle + 1, right, x);
			}
		else {
			return square(left, middle-1, x); 
		}
		
	}

}
